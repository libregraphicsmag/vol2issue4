Practical & Useful

There's an adage in the software world: programs should do one thing very well. In that spirit, we offer you a round-up of small and useful programs and resources which do one thing particularly well. This issue, we highlight some tools for capturing the world around you.

VisualSFM
http://ccwu.me/vsfm
[Insert descriptive text. It basically does photogrammetry.]

Way Back Machine
https://archive.org/web
[time machine for webpages]

Textfiles.com
http://textfiles.com
[ascii art + bbs + text adventures]

sK1 Palette Collection
http://sk1project.org/palettes.php
A collection of color compilations, ranging from Android UI to browser-safe colors, released to the public domain by the sK1 Project team.

Hybrid Publishing Resources
https://gitlab.com/DigitalPublishingToolkit/Hybrid-Publishing-Resources/wikis/home
Part of the Digital Publishing Toolkit, this wiki gathers many hacks, shortcuts and advice for creating publications using contemporary practices for digital and print outputs.

Setting up a hidden volume
https://flossmanuals.net/hide-your-data-workbook/setting-up-a-hidden-volume/
Part of the "Hide Your Data" book from Floss Manuals, this is a quick guide to easily set up encrypted partitions and keep your hard drive safe from prying eyes.

LOLcommits
https://github.com/mroth/lolcommits
Get a snapshot of yourself at every time you commit code, in true LOLcat style. "Git blame has never been so much fun!"

OpenStreetMap Traces
https://www.openstreetmap.org/traces
A repository of GPS tracks and subjective paths, open for uploading your own.
