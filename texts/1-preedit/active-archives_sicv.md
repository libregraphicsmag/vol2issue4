[[File:GA LH 000014 C WEB1.layers.preview.png|320px]] [[File:Zzscan 2015-04-17T17-24-07Z.layers.preview.png|320px]]

An image is more than the sum of its pixels.

In the context of the traditional art school, we are taught to distrust the "effects" of the photo editing software. Why use a digital simulation when you can work with the "true materials" of paint? What are the "true materials" of software, and hadn't we better use those when considering what painting means on a digital canvas? How can digital tools embrace the actual material of the algorithmic rather than merely simulating the analog?

The more we investigated "computer vision" techniques, however, the more we realized computer scientists are using the same techniques, and even employing an approach where techniques are composed almost as the cutups of a visual collage. No time to compute an actual Laplacian of gaussian? No problem, the textbooks offer, just gaussian blur at multiples of the square root of 2 and subtract the results to get a pretty good approximation.

A scanned image is more than a matrix of color values destined only to be displayed as pixels. Viewed through the lens of algorithms, an image is multiple layers of potential interpretations. Each layer tells different story, revealing some aspects, obscuring others. Algorithmic glitches are revealing: is this a letter, or the edge of a roof tile? What are the visual features of text when treated again as an image? The layers also speak to each other; how do SIFT features related to edges, and edges to the automatic detection of text in an OCR software?

Legend (description of the layers)
* red, green, blue: What is a significant color information? Contrary to human intuition, for a computer, a white image is an image saturated with red, blue and green. To find the images that look the most blueish, that appear most red or green we counted only the color values that were superior to the others by a certain threshold.
* contours: shows the contours detected on each image. In addition to tracing the edges, the algorithm connects the lines into a series of distinct segments represented by a different color.
* SIFT: SIFT (for scale-invariant feature transform) features are “interesting” points of an image that can be extracted to provide its "feature description". This description can then be used to identify (parts of) an image even when rotated or changed.
* Lexicality and Texture: These layers are produced using Tesseract, a software for optical character recognition (OCR). An OCR program operates at different levels of granularity. It can detect lines, words, symbols. Lexicality shows the words detected in an image, while Texture shows the symbols detected. Texture is configured to be rather tolerant in its understanding of what a character is. It therefore tends to see characters in very unexpected places.
 
Sources & Image credits:
* Left Image: Leaf from Schedel's Weltchronik. Printed by Anton Koberger in Nuremberg, 1493. From the collection of Guttorm Guttormsgaard.
* Right Image: Scan of the first page of the essay "[http://www.obs-osv.com/bilder/Report%20from%20the%20Gutenberg%20Galaxy_1.pdf Digital Textuality: The Implicit (Re-)Turn of the Gutenberg Galaxy]" by Wolfgang Ernst with photograph. From Report 1 from the Gutenberg Galaxy, Blaker, Norway.
http://www.obs-osv.com/bilder/Report%20from%20the%20Gutenberg%20Galaxy_1.pdf

Scandinavian Institute for Computational Vandalism
http://sicv.activearchives.org
