Small & useful

There's an adage in the software world: programs should do one thing very well. In that spirit, we offer you a round-up of small and useful programs and resources which do one thing particularly well. This issue, we highlight some tools for capturing the world around you.

VisualSFM
http://ccwu.me/vsfm
[Insert descriptive text. It basically does photogrammetry.]

Way Back Machine
https://archive.org/web
[time machine for webpages]

Textfiles.com
http://textfiles.com
[ascii art + bbs + text adventures]
