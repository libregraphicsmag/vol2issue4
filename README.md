# Capture, Issue 2.4

## Index

* **Capturing Capture**, ginger coon — *Editor's letter*
* **Outils Libres Alternatifs** — *Notebook*
* **Dry layout**, Manufactura Independente — *Production colophon*
* *New Releases*
* **Evasive Maneuvers**, Antonio Roberts — *Column*
* **Tonight we’re making web sites like it’s 1999**, Eric Schrijver — *Column*
* **+rwx data and pipes: Better design for otherwise non‑writable infrastructure and unreadable data**, Birgit Bachler and Walter Langelaar — *In practice*
* **Fons, A recipe to make fonts out of bitmap images**, Stéphanie Vilayphiou — *Dispatch*
* **Active Archives**, Scandinavian Institute for Computational Vandalism — *Showcase*
* **Search by Image**, Sebastian Schmieg — *Showcase*
* **Printing Out The Internet**, Kenneth Goldsmith — *Showcase*
* **Bring a thing!**, Robert M Ochshorn — *Feature*
* *Practical & Useful*
* **On capture [ a semiotic meditation ]**, Jessica Fenlon — *Essay*
* **Open Color 3d Scan**, Anna Carreras, Carles Domènech and Mariona Roca — *Interview*
* *Resources & Glossary*

## Colophon

Capture — October 2015  
Issue 2.4, [Libre Graphics Magazine](http://libregraphicsmag.com)  
ISSN: 1925-1416

**Editorial Team:**  
* [Ana Isabel Carvalho](http://manufacturaindependente.org)
* [ginger coons](http://adaptstudio.ca)
* [Ricardo Lafuente](http://manufacturaindependente.org)

**Copy editor:**  
Margaret Burnett  

**Publisher:**  
ginger coons  

**Community Board:**  
Dave Crossland, Louis Desjardins, Aymeric Mansoux, Alexandre Prokoudine, Femke Snelting  

**Contributors:**  
* [Birgit Bachler](http://www.birgitbachler.com/portfolio/)
* [Anna Carreras](http://www.annacarreras.com/eng/)
* [Carles Domènech](http://www.opencolor3dscan.com/)
* [Jessica Fenlon](http://www.drawclose.com/)
* [Kenneth Goldsmith](http://printingtheinternet.tumblr.com/)
* [Walter Langelaar](http://lowstandart.net/)
* [Robert M Ochshorn](http://rmozone.com/)
* [Outils Libres Alternatifs](http://outilslibresalternatifs.org)
* [Antonio Roberts](http://www.hellocatfood.com/)
* [Mariona Roca](http://opencolor3dscan.com)
* [Sebastian Schmieg](http://sebastianschmieg.com/)
* [Eric Schrijver](http://ericschrijver.nl/)
* [Stéphanie Vilayphiou](http://stdin.fr/)
* [Scandinavian Institute for Computational Vandalism](http://sicv.activearchives.org/c/)

Printed in Porto by [Cromotema](http://www.cromotema.pt) on recycled paper.

Licensed under a [Creative Commons Attribution-ShareAlike license (CC BY-SA)](https://creativecommons.org/licenses/by-sa/3.0/).  
All content should be attributed to its individual author.  
All content without a stated author can be credited to Libre Graphics Magazine.

Write to us at [enquiries@libregraphicsmag.com](enquiries@libregraphicsmag.com)  
Our repositories with all source material for the magazine can be found at [www.gitlab.com/libregraphicsmag](https://gitlab.com/libregraphicsmag)

## Relevant links

* [Public announcement](http://libregraphicsmag.com/2015/12/announcing-libre-graphics-magazine-issue-2-4-capture)
* [Call for submissions](http://libregraphicsmag.com/2015/03/call-for-submissions-libre-graphics-magazine-2-4)
* [PDF Hi-res](http://libregraphicsmag.com/files/libregraphicsmag_2.4_highquality.pdf) — download for print (140 MB)
* [PDF Low-res](http://libregraphicsmag.com/files/libregraphicsmag_2.4_lowquality.pdf) — download for screen (25 MB)
* [Git repository](https://gitlab.com/libregraphicsmag/vol2issue4/)

## Repository structure

This repository contains the source files used in the production of issue 2.4 of Libre Graphics Magazine.  
*Some of the files used in this issue can be found at [Persistent](https://gitlab.com/libregraphicsmag/persistent), a repository that contains reusable assets such as the magazine logo, columnist photos, procedures and procedures.*

* Assets folder: contains all the visual assets used  in this issue, organised by article. Here you'll find bitmap images, vector files and ads.
* Layout folder: Scribus layout documents. The layout is split in two files: part1 and part2.  
* Text: one .TXT file for each article. This folder is subdivided in:
  * preedit: article received, first version, without edits.
  * firsedit: articles after the first edit.
  * secondedit: final version of the articles published. 
* Files in the root of the repository:
  * 2point4call.txt: call for submissions for this issue.
  * image credits: credits for the images used in this issue.
  * spreads-color.ods: index of articles and pairs for printing (colour and b&W page sorting)

## Attribution and Licenses of individual files

**Images under a CC Attribution-ShareAlike license:**  
Cover and showcase separator by Manufactura Independente.  
Photo of ginger coons by herself.  
Photo of Antonio Roberts by Emily Davies.  
Photo of Eric Schrijver by himself.  
All the images in the Dispatch are by Stéphanie Vilayphiou.  
All the images in In Practice are by Birgit Bachler and Walter Langelaar.  
All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.  
Photos in the article Search by Image, in pages 30-32, are by Thomas Spallek. The image in page 33 is by Sebastien Schmieg.  
Photos in the article Printing Out the Internet are by Marisol Rodríguez, courtesy of LABOR.
[Image of a gun](https://commons.wikimedia.org/wiki/File:Glock_17C_cropped.jpg) in the essay On Capture is by Wikipedia user Nukes4Tots.

**Images and assets under other licenses:**  
Graphics in Index and Practical & Useful are based in an [image from page 29](https://www.flickr.com/photos/internetarchivebookimages/14592415089) of the book Practical Wire Rope Information and Useful Information on the Drag-line Cableway Excavators, under the Public Domain.  
Illustration for Antonio Roberts’s column is a modified version of [Closed Eye](https://thenounproject.com/search/?q=eyes&i=98222) by Yaroslav Samoilov, under CC BY.  
Illustration in New Releases is based in [Haloir, ou séchoir, pour la fabrication du fromage de Camembert](https://www.flickr.com/photos/fdctsevilla/4305560559), from the book Les Merveilles de l’Industrie ou, Description des Principales Industries Moderne, by Louis Figuier, under CC BY.
Images for Active Archives, by Scandinavian Institute for Computational Vandalism (SIC)V are under the [Free Art License](http://artlibre.org/licence/lal/en/). The original images are: Leaf, from Schedel’s Weltchronik, printed by Anton Koberger in Nuremberg (1493), from the collection of Guttorm Guttormsgaard; scan of the first page of the essay [Digital Textuality: The Implicit (Re-)Turn of the Gutenberg Galaxy](http://www.obs=osv.com/bilder/Report%20from%20the%20Gutenberg%20Galaxy_1.pdf), by Wolfgang Ernst.  
Image of [Storm on the Sea of Galilee](https://commons.wikimedia.org/wiki/File:Rembrandt_Christ_in_the_Storm_on_the_Lake_of_Galilee.jpg), by Rembrandt, in the essay On Capture, is under the Public Domain.

**General**  
Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license.   
It is best to check with the projects they represent before reusing them.